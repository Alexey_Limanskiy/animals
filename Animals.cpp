#include <iostream>
#include <string>

using namespace std;

class Animal
{
      
public:

    virtual void Voce()
    {

     cout << "Do you like animals?" << endl;

    }

};

class Dog : public Animal
{
public:

    void Voce() override
    {

        cout << "Woof!" << endl;

    }


};


class Cat : public Animal
{
public:

    void Voce() override
    {

        cout << "May!" << endl;

    }


};


class Skunk : public Animal
{
public:

    void Voce() override
    {

        cout << "Fuuux!" << endl;

    }


};

int main()
{
    setlocale(LC_ALL, "ru");

    //Animal one;
    
    const int SIZE = 3;
    
    
    Animal *voises[SIZE] = {new Dog,new Cat, new Skunk};

    for (int i = 0; i < 3; i++)
    {
        voises[i]->Voce(); 
                            
        delete new Dog; 
        delete new Cat;
        delete new Skunk;
    }

   
    return 0;
   
}


